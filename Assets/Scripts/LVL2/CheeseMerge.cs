using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheeseMerge : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Cheese")
        {
            Destroy(gameObject);
            WinLevel();
        }
    }

    private void WinLevel()
    {
        
    }
}
