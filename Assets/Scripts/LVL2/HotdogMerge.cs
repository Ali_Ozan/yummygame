using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotdogMerge : MonoBehaviour
{
    public GameObject Hamburger;
    public bool alreadyCreatedNewObject;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Hotdog")
        {
            if (alreadyCreatedNewObject)
            {
                Destroy(gameObject);
            }
            else
            {
                alreadyCreatedNewObject = true;
                collision.gameObject.GetComponent<HotdogMerge>().alreadyCreatedNewObject = true;
                Destroy(gameObject);
                Instantiate(Hamburger, collision.contacts[0].point, Quaternion.identity);
            }
        }
    }
}

