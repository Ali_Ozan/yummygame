using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HamburgerMerge : MonoBehaviour
{
    public GameObject Cheese;
    public bool alreadyCreatedNewObject;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Hamburger")
        {
            if (alreadyCreatedNewObject)
            {
                Destroy(gameObject);
            }
            else
            {
                alreadyCreatedNewObject = true;
                collision.gameObject.GetComponent<HamburgerMerge>().alreadyCreatedNewObject = true;
                Destroy(gameObject);
                Instantiate(Cheese, collision.contacts[0].point, Quaternion.identity);
            }
        }
    }
}
