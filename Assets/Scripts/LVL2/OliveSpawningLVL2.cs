using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveSpawningLVL2 : MonoBehaviour
{
    public GameObject OliveLVL2;
    private int xPos;
    private int zPos;
    private int oliveQuantity = 0;
    List<Vector3> olivePositions = new List<Vector3>();
    Vector3 trialPosition;
    void Start()
    {
        StartCoroutine(SpawnOlives());
    }

    IEnumerator SpawnOlives()
    {
        while (oliveQuantity < 32)
        {
            if (GeneratedNewOlivePosition())
            {
                Instantiate(OliveLVL2, trialPosition, Quaternion.identity);
                oliveQuantity++;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    private bool GeneratedNewOlivePosition()
    {
        xPos = Random.Range(-6, 6);
        zPos = Random.Range(-10, 12);
        trialPosition = new Vector3(xPos, 0, zPos);
        for (int i = 0; i < olivePositions.Count; i++)
        {
            if ((Vector3.Distance(olivePositions[i], trialPosition)) < 1.5f)
                return false;
        }
        olivePositions.Add(trialPosition);
        return true;
    }
}
