using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BananaMerge : MonoBehaviour
{
    public GameObject Hotdog;
    public bool alreadyCreatedNewObject;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Banana")
        {
            if (alreadyCreatedNewObject)
            {
                Destroy(gameObject);
            }
            else
            {
                alreadyCreatedNewObject = true;
                collision.gameObject.GetComponent<BananaMerge>().alreadyCreatedNewObject = true;
                Destroy(gameObject);
                Instantiate(Hotdog, collision.contacts[0].point, Quaternion.identity);
            }
        }
    }
}
