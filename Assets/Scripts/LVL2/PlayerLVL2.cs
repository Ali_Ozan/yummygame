using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLVL2 : MonoBehaviour
{
    Rigidbody _rigidBody;
    Camera _camera;
    
    Collider planeCollider;
    Collider boundaryCollider1;
    Collider boundaryCollider2;
    Collider boundaryCollider3;
    Collider boundaryCollider4;
    Ray ray; //the line going from the camera through the mouse and through the plane
    RaycastHit rayHit; //the point where the ray intersects with the plane

    private void Awake()
    {
        FindObjectOfType<GameManager>().currentLVL = 2;
    }

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        planeCollider = GameObject.Find("WoodPlane").GetComponent<Collider>();
        boundaryCollider1 = GameObject.Find("InvisibleBoundary").GetComponent<Collider>();
        boundaryCollider2 = GameObject.Find("InvisibleBoundary (1)").GetComponent<Collider>();
        boundaryCollider3 = GameObject.Find("InvisibleBoundary (2)").GetComponent<Collider>();
        boundaryCollider4 = GameObject.Find("InvisibleBoundary (3)").GetComponent<Collider>();
    }

    private void Update()
    {
        //transform.position = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.5f));
        if (Input.GetMouseButton(0))
        {
            ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out rayHit)) //checking if the ray actually intersects with the plane
            {
                if ((rayHit.collider == planeCollider) ||
                    (rayHit.collider == boundaryCollider1) ||
                    (rayHit.collider == boundaryCollider2) ||
                    (rayHit.collider == boundaryCollider3) ||
                    (rayHit.collider == boundaryCollider4))
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(rayHit.point.x,0f, rayHit.point.z), Time.deltaTime * 10); //moving towards the point the ray hit
                    transform.LookAt(new Vector3(rayHit.point.x, transform.position.y, rayHit.point.z));
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Cheese")
        {
            Destroy(gameObject);
        }
    }
}
