using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveMerge : MonoBehaviour
{
    public GameObject Cherry;
    public bool alreadyCreatedNewObject;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Olive")
        {
            if (alreadyCreatedNewObject)
            {
                Destroy(gameObject);
            }
            else
            {
                alreadyCreatedNewObject = true;
                collision.gameObject.GetComponent<OliveMerge>().alreadyCreatedNewObject = true;
                Destroy(gameObject);
                Instantiate(Cherry, collision.contacts[0].point, Quaternion.identity);
            }
        }
    }
}
