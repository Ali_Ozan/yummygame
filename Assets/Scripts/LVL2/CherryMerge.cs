using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryMerge : MonoBehaviour
{
    public GameObject Banana;
    public bool alreadyCreatedNewObject;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Cherry")
        {
            if (alreadyCreatedNewObject)
            {
                Destroy(gameObject);
            }
            else
            {
                alreadyCreatedNewObject = true;
                collision.gameObject.GetComponent<CherryMerge>().alreadyCreatedNewObject = true;
                Destroy(gameObject);
                Instantiate(Banana, collision.contacts[0].point, Quaternion.identity);
            }
        }
    }
}
