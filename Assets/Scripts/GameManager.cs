using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public GameObject inventoryPanel;
    public GameObject playPanel;
    public GameObject levelCompletedPanel;
    public GameObject giftPanel;

    public GameObject OliveBall;

    public static GameManager Instance;

    public enum State { MENU, PLAY, INVENTORY, LEVELCOMPLETED, CHANGINGLEVEL};
    State _state = State.PLAY;

    int colledtedOlive = 0;
    int colledtedCherry = 0;
    int colledtedBanana = 0;
    int colledtedHotdog = 0;
    int colledteHamburger = 0;
    int colledtedCheese = 0;
    int colledtedWatermelon = 0;

    public Text giftName;

    public Text oliveCount;
    public Text cherryCount;
    public Text bananaCount;
    public Text hotdogCount;
    public Text hamburgerCount;
    public Text cheeseCount;
    public Text watermelonCount;

    bool givenTheLevelGift = false;
    bool _isSwitchingState = false;
    public int currentLVL = 0;
    public bool lvl1Completed = false;

    void Start()
    {
        UpdateCountTexts();
        Instance = this;
    }

    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                break;
            case State.PLAY:
                switch (currentLVL)
                {
                    case 0:
                        break;
                    case 1:
                        if (lvl1Completed)
                        {
                            SwitchState(State.LEVELCOMPLETED, 1f);
                        }
                        break;
                    case 2:
                        if (GameObject.Find("PlayerLVL2") == null)
                        { 
                            SwitchState(State.LEVELCOMPLETED);
                        }
                        break;
                    case 3:
                        SwitchState(State.LEVELCOMPLETED, 2f);
                        break;
                    case 4:
                        if (GameObject.Find("Bricks").GetComponent<Transform>().childCount == 0) //All fruits have been destroyed
                        {
                            Destroy(OliveBall);
                            SwitchState(State.LEVELCOMPLETED, 1f);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case State.INVENTORY:
                BeginState(State.INVENTORY);
                break;
            case State.LEVELCOMPLETED:
                BeginState(State.LEVELCOMPLETED);
                break;
            case State.CHANGINGLEVEL:
                break;
        }
        
    }

    public void SwitchState(State newState, float delay = 0)
    {
        StartCoroutine(SwitchDelay(newState, delay));
    }

    IEnumerator SwitchDelay(State newState, float delay)
    {
        _isSwitchingState = true;
        yield return new WaitForSeconds(delay);
        EndState();
        _state = newState;
        BeginState(newState);
        _isSwitchingState = false;
    }
    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.PLAY:
                break;
            case State.INVENTORY:
                inventoryPanel.SetActive(true);
                break;
            case State.LEVELCOMPLETED:
                levelCompletedPanel.SetActive(true);
                if (!givenTheLevelGift)
                {
                    GiveGift();
                }
                Invoke("ShowInventory", 7f);
                Invoke("LoadNextLevel", 11f);
                SwitchState(State.CHANGINGLEVEL);
                break;
        }
    }

    private void GiveGift()
    {
        givenTheLevelGift = true;
        Debug.Log(givenTheLevelGift);
        giftPanel.SetActive(true);
        int randomFruit = UnityEngine.Random.Range(1, 8);
        switch (randomFruit)
        {
            case 1:
                giftName.text = "AN OLIVE!";
                colledtedOlive++;
                break;
            case 2:
                giftName.text = "A CHERRY!";
                colledtedCherry++;
                break;
            case 3:
                giftName.text = "A BANANA!";
                colledtedBanana++;
                break;
            case 4:
                giftName.text = "A HOTDOG!";
                colledtedHotdog++;
                break;
            case 5:
                giftName.text = "A HAMBURGER!";
                colledteHamburger++;
                break;
            case 6:
                giftName.text = "A CHEESE!";
                colledtedCheese++;
                break;
            case 7:
                giftName.text = "A WATERMELON!";
                colledtedWatermelon++;
                break;
        }
        UpdateCountTexts();
    }

    private void UpdateCountTexts()
    {
        oliveCount.text = colledtedOlive.ToString();
        cherryCount.text = colledtedCherry.ToString();
        bananaCount.text = colledtedBanana.ToString();
        hotdogCount.text = colledtedHotdog.ToString();
        hamburgerCount.text = colledteHamburger.ToString(); 
        cheeseCount.text = colledtedCheese.ToString();
        watermelonCount.text = colledtedWatermelon.ToString();
    }

    void EndState()
    {
        switch (_state)
        {
            case State.PLAY:
                break;
            case State.INVENTORY:
                break;
            case State.LEVELCOMPLETED:
                //levelCompletedPanel.SetActive(false);
                break;
        }

    }

    void LoadNextLevel()
    {
        if(currentLVL != (SceneManager.sceneCountInBuildSettings - 1)) //if it's not the next level load next. if it is the last level start from the beginning
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else
            SceneManager.LoadScene(1);
        givenTheLevelGift = false;
    }

    void ShowInventory()
    {
        inventoryPanel.SetActive(true);
        giftPanel.SetActive(false);
    }
}
