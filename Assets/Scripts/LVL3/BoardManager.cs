﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardManager : MonoBehaviour
{
    public GameObject Cherry;
    public GameObject Banana;
    public GameObject Watermelon;

    public List<GameObject> RowsList;
    public List<GameObject> ColumnsList;

    void Start()
    {
        FindObjectOfType<GameManager>().currentLVL = 3;
        var levelString = Resources.Load<TextAsset>("Level3Data").text;
        Debug.Log(levelString);


        var boardData = JsonUtility.FromJson<BoardData>(levelString);

        GameObject Rows = new GameObject();
        Rows.name = "Rows";
        GameObject Columns = new GameObject();
        Columns.name = "Columns";

        for (int y = 0; y < boardData.Y.Length; y++)
        {
            GameObject row = new GameObject();
            row.name = "Row" + (y + 1).ToString();
            row.transform.parent = Rows.transform;
            RowsList.Add(row);
        }

        for (int x = 0; x < boardData.Y[0].X.Length; x++)
        {
            GameObject column = new GameObject();
            column.name = "Column" + (x + 1).ToString();
            column.transform.parent = Columns.transform;
            ColumnsList.Add(column);
        }


        for (int y = 0; y < boardData.Y.Length; y++)
        {
            for (int x = 0; x < boardData.Y[y].X.Length; x++)
            {
                var id = boardData.Y[y].X[x];
                GameObject prefab = Cherry;

                if (id == 1)
                    prefab = Banana;
                else if (id == 2)
                    prefab = Watermelon;

                Instantiate(prefab, new Vector3((x - 2) * 3.3f, 0f, 10 - (y * 4)), Quaternion.identity, GameObject.Find("Row" + (y + 1).ToString()).transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}