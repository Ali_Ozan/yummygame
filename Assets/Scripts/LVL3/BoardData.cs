﻿using UnityEngine;

[System.Serializable]
public class BoardData
{
    [SerializeField] private Y[] _y = new Y[1];

    public Y[] Y
    {
        get => _y;
        set => _y = value;
    }
}

[System.Serializable]
public class Y
{
    [SerializeField] private int[] _x = new int[1];

    public int[] X
    {
        get => _x;
        set => _x = value;
    }
}