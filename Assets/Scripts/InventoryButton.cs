using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{
    public GameObject inventoryPanel;
    public void OpenInventory()
    {
        if (inventoryPanel.activeInHierarchy)
        {
            inventoryPanel.SetActive(false);
            Time.timeScale = 1f;
        }
        else
        {
            inventoryPanel.SetActive(true);
            Time.timeScale = 0f;
        }
    }
}
