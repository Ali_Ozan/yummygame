using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLVL1 : MonoBehaviour
{
    Rigidbody _rigidBody;
    Camera _camera;
    
    Collider planeCollider;
    Collider boundaryCollider1;
    Collider boundaryCollider2;
    Collider boundaryCollider3;
    Collider boundaryCollider4;
    Ray ray; //the line going from the camera through the mouse and through the plane
    RaycastHit rayHit; //the point where the ray intersects with the plane

    private float _speed = 30f;

    private void Awake()
    {
        FindObjectOfType<GameManager>().currentLVL = 1;
    }

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        planeCollider = GameObject.Find("WoodPlane").GetComponent<Collider>();
        boundaryCollider1 = GameObject.Find("InvisibleBoundary").GetComponent<Collider>();
        boundaryCollider2 = GameObject.Find("InvisibleBoundary (1)").GetComponent<Collider>();
        boundaryCollider3 = GameObject.Find("InvisibleBoundary (2)").GetComponent<Collider>();
        boundaryCollider4 = GameObject.Find("InvisibleBoundary (3)").GetComponent<Collider>();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out rayHit)) //checking if the ray actually intersects with the plane
            {
                if ((rayHit.collider == planeCollider) ||
                    (rayHit.collider == boundaryCollider1) ||
                    (rayHit.collider == boundaryCollider2) ||
                    (rayHit.collider == boundaryCollider3) ||
                    (rayHit.collider == boundaryCollider4))
                {
                    Vector3 targetPoint = new Vector3(rayHit.point.x, transform.position.y, rayHit.point.z);
                    _rigidBody.velocity = (targetPoint - _rigidBody.transform.position).normalized * _speed;
                    Vector3 movingDirection = new Vector3(rayHit.point.x, transform.position.y, rayHit.point.z);
                    transform.LookAt(movingDirection);
                }
            }
        }
    }
}
