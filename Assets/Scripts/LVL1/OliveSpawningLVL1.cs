using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveSpawningLVL1 : MonoBehaviour
{
    public GameObject Olive;
    private int xPos;
    private int yPos;
    private int zPos;
    public int olivesToSpawn = 64;
    private int oliveQuantity = 0;
    
    void Start()
    {
        StartCoroutine(SpawnOlives());
    }

    IEnumerator SpawnOlives()
    {
        while (oliveQuantity < olivesToSpawn)
        {
            xPos = Random.Range(-5, 5);
            yPos = Random.Range(1, 3);
            zPos = Random.Range(-4, 9);
            Instantiate(Olive, new Vector3(xPos, yPos, zPos), Quaternion.identity);
            oliveQuantity++;
            yield return new WaitForSeconds(0.01f);
        }
    }
}
