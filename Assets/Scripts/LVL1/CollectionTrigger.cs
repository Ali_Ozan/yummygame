using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionTrigger : MonoBehaviour
{
    private int collectedItemCount = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Olive")
        {
            collectedItemCount++;
            Debug.Log(collectedItemCount.ToString());
            StartCoroutine(LockTheObject(other));
        }
    }

    IEnumerator LockTheObject(Collider other)
    {
        yield return new WaitForSeconds(0.8f);
        other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        other.gameObject.GetComponent<Collider>().enabled = false;
        other.gameObject.GetComponent<MeshRenderer>().material.color = new Color(0.56f, 1f, 0.86f);
    }

    private void Update()
    {
        if (collectedItemCount == FindObjectOfType<OliveSpawningLVL1>().olivesToSpawn)
        {
            FindObjectOfType<GameManager>().lvl1Completed = true;
        }
    }
}
