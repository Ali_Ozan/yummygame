using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotdogPaddle : MonoBehaviour
{
    Rigidbody _rigidBody;
    Vector3 _initialPosition;
    Camera _camera;

    Collider planeCollider;
    Ray ray; //the line going from the camera through the mouse and through the plane
    RaycastHit rayHit; //the point where the ray intersects with the plane
    public float additionalBumperDistance = 0; //this will be useful when hitting a banana elongates the paddle
    public int directionMultiplier = 1; //this will be multiplied with -1 each time when a hamburger is hit 

    private void Awake()
    {
        FindObjectOfType<GameManager>().currentLVL = 4;
    }

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _initialPosition = _rigidBody.position;
        _camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        planeCollider = GameObject.Find("WoodPlane").GetComponent<Collider>();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out rayHit)) //checking if the ray actually intersects with the plane
            {
                if (rayHit.collider == planeCollider)
                {
                    Vector3 targetPosition = new Vector3(rayHit.point.x * directionMultiplier, transform.position.y, transform.position.z);
                    if (targetPosition.x > (6.5f - additionalBumperDistance)) //in order to avoid going off limits
                        targetPosition.x = (6.5f - additionalBumperDistance);
                    if (targetPosition.x < (-6.5f + additionalBumperDistance))
                        targetPosition.x = (-6.5f + additionalBumperDistance);
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * 35); //moving towards the point the ray hit
                }
            }
        }
    }
}
