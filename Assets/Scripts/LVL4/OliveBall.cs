using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveBall : MonoBehaviour
{
    float _speed = 10f;
    Rigidbody _rigidBody;
    Renderer _renderer;
    Vector3 _velocity;
    Vector3 _initialPosition;
    GameObject _hotdogPaddle;
    Color originalColor;

    private float noHitRemainingTime = 15f;//this time will be tracked in case of the ball going horizontal and not hitting anything. If the time is up, a small kick will change its direction
    private bool firstLaunchHappened = false; // this boolean is added because of a bug that happens when the object renderer is sometimes not visible at the few microseconds at the beginning of the game and caused invoking multiple launches 
    private float wobbleTimeRemaining = 0f;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _initialPosition = _rigidBody.position;
        Invoke("Launch", 1f);
        _renderer = GetComponent<Renderer>();
        originalColor = _renderer.material.color;
        _hotdogPaddle = GameObject.Find("PlayerLVL4");
    }

    void Launch()
    {
        firstLaunchHappened = true;
        int xDir = Random.Range(-3, 3);
        int zDir = Random.Range(-1, -3);
        _rigidBody.velocity = new Vector3(xDir, 0, zDir).normalized * _speed;
        //_rigidBody.velocity = new Vector3(1, 0, 0).normalized * _speed;
    }
    void FixedUpdate()
    {
        if (wobbleTimeRemaining > 0)
        {
            WobbleBall();
        }
        else
        {
            _rigidBody.velocity = _rigidBody.velocity.normalized * _speed;
            _velocity = _rigidBody.velocity;
        }

        if (!_renderer.isVisible && firstLaunchHappened) 
        {
            _rigidBody.velocity = new Vector3(0, 0, 0);
            _rigidBody.position = _initialPosition;
            noHitRemainingTime = 15f;
            Invoke("Launch", 1.5f);
        }

        noHitRemainingTime -= Time.deltaTime;
        if (noHitRemainingTime < 3)
            TurnBallRed();
        if (noHitRemainingTime <= 0)
        {
            KickTheBall();
            _renderer.material.color = originalColor;
        }
    }

    void TurnBallRed()
    {
        _renderer.material.color = Color.red;
    }

    void KickTheBall()
    {
        int xDir = Random.Range(-3, 3);
        int zDir = Random.Range(1, 3);
        _rigidBody.velocity = new Vector3(xDir, 0, zDir).normalized * _speed;
        noHitRemainingTime = 15;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 6 || collision.gameObject.layer == 7) //The time is reset if the collided object is a brick or the paddle
        {
            noHitRemainingTime = 15;
            _renderer.material.color = originalColor;
        }
        if (collision.gameObject.tag != "WoodPlane" && collision.gameObject.tag != "Olive")
        {
            _rigidBody.velocity = Vector3.Reflect(_velocity, collision.contacts[0].normal);
            if (collision.gameObject.tag == "Cherry")
            {
                Destroy(collision.gameObject);
                StartCoroutine(CherrySpeedUp());
            }
            if (collision.gameObject.tag == "Banana")
            {
                Destroy(collision.gameObject);
                StartCoroutine(ElongatePaddle());
            }
            if (collision.gameObject.tag == "Cheese")
            {
                Destroy(collision.gameObject);
                wobbleTimeRemaining += 3f; //for each cheese collected the wobbling time stacks up. Wobbling behaviour does not change
            }
            if (collision.gameObject.tag == "Watermelon")
            {
                Destroy(collision.gameObject);
                StartCoroutine(EnlargeBall());
            }
            if (collision.gameObject.tag == "Hamburger")
            {
                Destroy(collision.gameObject);
                ReversePaddleMovement();
            }
        }
    }

    IEnumerator CherrySpeedUp()
    {
        _speed *= 1.4f;
        yield return new WaitForSeconds(3);
        _speed /= 1.4f;
    }

    IEnumerator EnlargeBall()
    {
        _rigidBody.transform.localScale += new Vector3(3f, 3f, 3f);
        yield return new WaitForSeconds(3);
        _rigidBody.transform.localScale -= new Vector3(3f, 3f, 3f);
    }

    void WobbleBall()
    {
        wobbleTimeRemaining -= Time.deltaTime;
        float sinusVariable = Mathf.Sin(Time.time*20);
        float wobblingIntensity = 5f;
        Vector3 vectorPerpendicularToVelocity = new Vector3(-_velocity.z/_velocity.x, 0, 1);
        _rigidBody.velocity = (_rigidBody.velocity + vectorPerpendicularToVelocity * sinusVariable * wobblingIntensity).normalized * _speed * 1.3f;
    }

    IEnumerator ElongatePaddle()
    {
        _hotdogPaddle.transform.localScale += new Vector3(4f, 0f, 0f);
        _hotdogPaddle.GetComponent<HotdogPaddle>().additionalBumperDistance += 1;
        yield return new WaitForSeconds(3);
        _hotdogPaddle.transform.localScale -= new Vector3(4f, 0f, 0f);
        _hotdogPaddle.GetComponent<HotdogPaddle>().additionalBumperDistance -= 1;
    }

    void ReversePaddleMovement()
    {
        _hotdogPaddle.GetComponent<HotdogPaddle>().directionMultiplier *= -1;
    }
}
